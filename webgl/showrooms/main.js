var mainpath = '1Min/1S/_1';
//var mainpath = '1Min/2M/_1';
//var mainpath = '1Min/3L/_1';

//var mainpath = '2MCL/1S/_1';
//var mainpath = '2MCL/2M/_1';
//var mainpath = '2MCL/3L/_1';

//var mainpath = '3Scn/1S/_1';
//var mainpath = '3Scn/2M/_1';
//var mainpath = '3Scn/3L/_1';

//var mainpath = '4Loft/1S/_1';
//var mainpath = '4Loft/2M/_1';
//var mainpath = '4Loft/3L/_1';

//var mainpath = '5Cas/1S/_1';
//var mainpath = '5Cas/2M/_1';
//var mainpath = '5Cas/3L/_1';

//var mainpath = '6ECL/1S/_1';
//var mainpath = '6ECL/2M/_1';
//var mainpath = '6ECL/3L/_1';

//var mainpath = 'zTEST';


// scene size
var WIDTH = window.innerWidth;
var HEIGHT = window.innerHeight;

// camera
var VIEW_ANGLE = 45;
var ASPECT = WIDTH / HEIGHT;
var NEAR = 1;
var FAR = 20000;

var camera, scene, renderer;

var pickingGeometry;
var pickingMaterial;
var pickingTexture;

var container2, stats;

var cameraControls;



// GLOBAL TEXTURES

var UVchecker = 'textures/UV_Grid_Sm.jpg';

//'../_Showroom/' + mainpath

var ShadowPath = mainpath + '/shadow/_SHADOW_CompleteMap.jpg';


// BOTTOM

var BottomTxPath = 'TEXTURES/bottom/textured/004.jpg';
var BottomTxWidth = 1600;
var BottomTxHeight = 1600;

var BottomObjTx = mainpath + '/texture/_BOTTOM.obj';



// FRONT

var FrontTxPath = 'TEXTURES/front/textured/white.jpg';
var FrontTxWidth = 1000;
var FrontTxHeight = 1000;

var FrontObjTx = mainpath + '/texture/_FRONT.obj';



// LEFT

var LeftTxPath = 'TEXTURES/left/textured/white.jpg';
var LeftTxWidth = 1000;
var LeftTxHeight = 1000;

var LeftObjTx = mainpath + '/texture/_LEFT.obj';



// RIGHT

var RightTxPath = 'TEXTURES/right/textured/005.jpg';
var RightTxWidth = 1000;
var RightTxHeight = 1000;

var RightObjTx = mainpath + '/texture/_RIGHT.obj';



// BACK

var BackTxPath = 'TEXTURES/back/textured/white.jpg';
var BackTxWidth = 1000;
var BackTxHeight = 1000;

var BackObjTx = mainpath + '/texture/_BACK.obj';



// TOP

var TopTxPath = 'TEXTURES/top/textured/001.jpg';
var TopTxWidth = 1000;
var TopTxHeight = 1000;

var TopObjTx = mainpath + '/texture/_TOP.obj';



function init() {

  // renderer
  renderer = new THREE.WebGLRenderer(
    { antialias: true }
  );
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(WIDTH, HEIGHT);
  renderer.setClearColor( 0xffffff );

  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  // scene
  scene = new THREE.Scene();

  var geometry = new THREE.Geometry();
  pickingGeometry = new THREE.Geometry();

  pickingScene = new THREE.Scene();
  pickingTexture = new THREE.WebGLRenderTarget( window.innerWidth, window.innerHeight );
  pickingTexture.minFilter = THREE.LinearFilter;
  pickingTexture.generateMipmaps = false;


  // camera
  camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
  camera.position.set(-4000, 2000, 0);

  cameraControls = new THREE.OrbitControls(camera, renderer.domElement);
  cameraControls.target.set(0, 1000, 0);
  cameraControls.maxDistance = 5000;
  cameraControls.minDistance = 10;
  cameraControls.update();

  var container = document.getElementById('container');
  container.appendChild(renderer.domElement);

}

function fillScene() {

  // global loading manager
  var manager = new THREE.LoadingManager();
  manager.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1.2);

  scene.add(light);

  var spotLight = new THREE.SpotLight( 0xffffff, 1, 15000, Math.PI/5, 8, 1 );
  spotLight.position.set( 0, 1500, -5000 );
  spotLight.target.position.set ( 0, 1500, 0 );
  spotLight.scale.x = 5;

  spotLight.castShadow = true;
  spotLight.shadowDarkness = 0.9;

  scene.add(spotLight);


  // Grid

  /*var helper = new THREE.GridHelper(5000, 1000);
  scene.add(helper);*/

  // MA OBJ =D

  var onProgress = function (xhr) {
    if (xhr.lengthComputable) {
      var percentComplete = xhr.loaded / xhr.total * 100;
      //console.log( Math.round(percentComplete, 2) + '% downloaded' );
    }
  };

  var onError = function (xhr) {
  };

  THREE.Loader.Handlers.add(/\.dds$/i, new THREE.DDSLoader());


  // FURNITURE ALL___________________________________________________


  var materialFur = new THREE.MeshPhongMaterial( {

  } );

  var loader2 = new THREE.OBJMTLLoader();
  loader2.load(
    mainpath + '/furniture/_ALL.obj',
    mainpath + '/furniture/_ALL.mtl',
    function (object2) {

    scene.add(object2);

  }, onProgress, onError);




  // ALL SHADOWS model_______________________________________________


  var managerAllSh = new THREE.LoadingManager();
  managerAllSh.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureAllSh = new THREE.Texture();

  var loaderShAll = new THREE.ImageLoader(manager);
  loaderShAll.load(mainpath + '/shadow/_SHADOW_CompleteMap.jpg', function (image) {

    textureAllSh.image = image;
    textureAllSh.needsUpdate = true;

  });

  var materialAll = new THREE.MeshBasicMaterial( { map: textureAllSh } );
  materialAll.transparent = true;
  materialAll.blending = THREE.MultiplyBlending;

  // BOTTOM SHADOWS model

  var loaderAllShadow = new THREE.OBJMTLLoader(manager);
  loaderAllShadow.load(
    mainpath + '/shadow/_SHADOW.obj',
    mainpath + '/shadow/_SHADOW.mtl',
    function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {        child.material = materialAll;
      }

    });

      object.receiveShadow = true;

    scene.add(object);

  }, onProgress, onError);






  // BOTTOM TEXTURE _________________________________________________________

  var managerBottom = new THREE.LoadingManager();
  managerBottom.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureBottom = new THREE.Texture();

  var loaderTxBottom = new THREE.ImageLoader(manager);
  loaderTxBottom.load(BottomTxPath, function (image) {

    textureBottom.image = image;

    textureBottom.wrapS = textureBottom.wrapT = THREE.RepeatWrapping;

    textureBottom.repeat.x = 1000 / BottomTxWidth;
    textureBottom.repeat.y = 1000 / BottomTxHeight;

    textureBottom.needsUpdate = true;


  });

  var materialTxBottom = new THREE.MeshPhongMaterial( {
    map: textureBottom,
    specular: 0xa0a0a0,
    shininess: 15
  } );


  // BOTTOM TEXTURE MODEL

  var loaderObjBottom = new THREE.OBJLoader(manager);
  loaderObjBottom.load(BottomObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material = materialTxBottom;
      }


    });

    object.castShadow = false;
    object.receiveShadow = true;

    scene.add(object);

  }, onProgress, onError);

  // Highlight


  /*var highlightBottom = new THREE.OBJLoader(manager);

  pickingScene.add( new THREE.Mesh( pickingGeometry, pickingMaterial ) );

  loaderObjBottom.load(BottomObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material = new THREE.MeshPhongMaterial( {
          color: 0xffff00,
          wireframe: true,
          //map:textureAllSh
        });
        //child.material.transparent = true;
        //child.material.blending = THREE.MultiplyBlending;

      }

    });

    object.castShadow = false;
    object.receiveShadow = true;

    scene.add(object);

  }, onProgress, onError);*/






  // FRONT TEXTURE _________________________________________________________

  var managerFront = new THREE.LoadingManager();
  managerFront.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureFront = new THREE.Texture();

  var loaderTxtFront = new THREE.ImageLoader(manager);
  loaderTxtFront.load(FrontTxPath, function (image) {

    textureFront.image = image;
    textureFront.wrapS = textureFront.wrapT = THREE.RepeatWrapping;

    textureFront.repeat.x = 1000 / FrontTxWidth;
    textureFront.repeat.y = 1000 / FrontTxHeight;

    textureFront.needsUpdate = true;


  });

  // FRONT TEXTURE model

  var loaderObjFront = new THREE.OBJLoader(manager);
  loaderObjFront.load(FrontObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material.map = textureFront;
      }

    });

    scene.add(object);

  }, onProgress, onError);






  // LEFT TEXTURE _________________________________________________________

  var managerLeft = new THREE.LoadingManager();
  managerLeft.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureLeft = new THREE.Texture();

  var loaderTxtLeft = new THREE.ImageLoader(manager);
  loaderTxtLeft.load(LeftTxPath, function (image) {

    textureLeft.image = image;
    textureLeft.wrapS = textureLeft.wrapT = THREE.RepeatWrapping;

    textureLeft.repeat.x = 1000 / LeftTxWidth;
    textureLeft.repeat.y = 1000 / LeftTxHeight;

    textureLeft.needsUpdate = true;


  });

  // LEFT TEXTURE model

  var loaderObjLeft = new THREE.OBJLoader(manager);
  loaderObjLeft.load(LeftObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material.map = textureLeft;
      }

    });

    scene.add(object);

  }, onProgress, onError);







  // RIGHT TEXTURE _________________________________________________________

  var managerRight = new THREE.LoadingManager();
  managerRight.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureRight = new THREE.Texture();

  var loaderTxtRight = new THREE.ImageLoader(manager);
  loaderTxtRight.load(RightTxPath, function (image) {

    textureRight.image = image;
    textureRight.wrapS = textureRight.wrapT = THREE.RepeatWrapping;

    textureRight.repeat.x = 1000 / RightTxWidth;
    textureRight.repeat.y = 1000 / RightTxHeight;

    textureRight.needsUpdate = true;


  });

  // RIGHT TEXTURE model

  var loaderObjRight = new THREE.OBJLoader(manager);
  loaderObjRight.load(RightObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material.map = textureRight;
      }

    });

    scene.add(object);

  }, onProgress, onError);






  // BACK TEXTURE _________________________________________________________

  var managerBack = new THREE.LoadingManager();
  managerBack.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureBack = new THREE.Texture();

  var loaderTxtBack = new THREE.ImageLoader(manager);
  loaderTxtBack.load(BackTxPath, function (image) {

    textureBack.image = image;
    textureBack.wrapS = textureBack.wrapT = THREE.RepeatWrapping;

    textureBack.repeat.x = 1000 / BackTxWidth;
    textureBack.repeat.y = 1000 / BackTxHeight;

    textureBack.needsUpdate = true;


  });

  // BACK TEXTURE model

  var loaderObjBack = new THREE.OBJLoader(manager);
  loaderObjBack.load(BackObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material.map = textureBack;
      }

    });

    scene.add(object);

  }, onProgress, onError);








  // TOP TEXTURE _________________________________________________________

  var managerTop = new THREE.LoadingManager();
  managerTop.onProgress = function (item, loaded, total) {
    console.log(item, loaded, total);
  };

  var textureTop = new THREE.Texture();

  var loaderTxtTop = new THREE.ImageLoader(manager);
  loaderTxtTop.load(TopTxPath, function (image) {

    textureTop.image = image;
    textureTop.wrapS = textureTop.wrapT = THREE.RepeatWrapping;

    textureTop.repeat.x = 1000 / TopTxWidth;
    textureTop.repeat.y = 1000 / TopTxHeight;

    textureTop.needsUpdate = true;


  });

  // TOP TEXTURE model

  var loaderObjTop = new THREE.OBJLoader(manager);
  loaderObjTop.load(TopObjTx, function (object) {

    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material.map = textureTop;
      }

    });

    scene.add(object);

  }, onProgress, onError);






}

function render() {
  renderer.render(scene, camera);
}

function update() {

  requestAnimationFrame(update);

  cameraControls.update();

  render();
}

init();
fillScene();
update();
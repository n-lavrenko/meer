import {servicesConfig} from './services.config';
import {statusSrv} from './status.service';
import {authHelper} from './auth.helper';

export default angular.module('app.core.services', ['satellizer'])
  .config(servicesConfig)
  .service('statusSrv', statusSrv)
  .service('authHelper', authHelper);

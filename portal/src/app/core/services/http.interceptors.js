export function myHttpInterceptor($q, $injector) {
  'ngInject';

  return {
    'request': (config) => {
      // do something on success
      //constants.baseURL
      return config;
    },

    'requestError': (rejection) => {
      // do something on error
      //if (canRecover(rejection)) {
      //  return responseOrNewPromise
      //}
      return this.$q.reject(rejection);
    },



    'response': function(response) {
      // do something on success
      if (response.config.url.indexOf('/api/') !== -1) {
        let $auth = $injector.get('$auth');
        $auth.setToken(response.headers('Access-Token'));
      }
      return response;
    },

    'responseError': function(rejection) {
      // do something on error
      //if (canRecover(rejection)) {
      //  return responseOrNewPromise
      //}
      return $q.reject(rejection);
    }
  }
}

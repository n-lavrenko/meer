export class statusSrv {
  constructor($http, baseURL, $q) {
    'ngInject';

    this.$http = $http;
    this.$q = $q;
    this.baseURL = baseURL;
    this.statuses = [];
  }

  getAll() {
    var deferred = this.$q.defer();

    if (this.statuses.length > 0) {
      deferred.resolve(this.statuses);
    }
    else {

      this.$http.get(this.baseURL + '/api/status')
        .then((res) => {
          this.statuses = res.data;
          deferred.resolve(res.data);
        })
        .catch((err) => {
          deferred.reject(err);
        });
    }

    return deferred.promise;
  }

}

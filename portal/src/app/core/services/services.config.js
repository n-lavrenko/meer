import {constants} from '../../app.constants';
import {myHttpInterceptor} from './http.interceptors';

export function servicesConfig($authProvider, $httpProvider) {
  'ngInject';

  // http handler
  $httpProvider.interceptors.push(myHttpInterceptor);


  // auth
  $authProvider.tokenName = 'token';
  $authProvider.authHeader = 'Access-Token';
  $authProvider.authToken = '';
  $authProvider.baseUrl = constants.baseURL;

  $authProvider.facebook({
    clientId: 'Facebook App ID'
  });

  $authProvider.google({
    clientId: 'AIzaSyAj3X0ppOSw-mZGqADxeF1tD9W36sONe2I'
  });

  $authProvider.github({
    clientId: 'GitHub Client ID'
  });

  $authProvider.linkedin({
    clientId: 'LinkedIn Client ID'
  });

  $authProvider.instagram({
    clientId: 'Instagram Client ID'
  });

  $authProvider.yahoo({
    clientId: 'Yahoo Client ID / Consumer Key'
  });

  $authProvider.live({
    clientId: 'Microsoft Client ID'
  });

  $authProvider.twitch({
    clientId: 'Twitch Client ID'
  });

  $authProvider.bitbucket({
    clientId: 'Bitbucket Client ID'
  });

  // No additional setup required for Twitter

  $authProvider.oauth2({
    name: 'foursquare',
    url: '/auth/foursquare',
    clientId: 'Foursquare Client ID',
    redirectUri: window.location.origin,
    authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate'
  });

}

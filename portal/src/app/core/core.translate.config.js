export function configTranslate($translateProvider) {
  'ngInject';

  $translateProvider.useSanitizeValueStrategy('sanitize');

  $translateProvider
    .translations('en', {

      // Define all menu elements
      ANALYTICS: 'Analytics',
      GRAPHS: 'Graphs',
      MAILBOX: 'Mailbox',
      WIDGETS: 'Widgets',
      METRICS: 'Metrics',
      FORMS: 'Forms',
      APPVIEWS: 'App views',
      OTHERPAGES: 'Other pages',
      UIELEMENTS: 'UI elements',
      MISCELLANEOUS: 'Miscellaneous',
      GRIDOPTIONS: 'Grid options',
      TABLES: 'Tables',
      PRODUCTS: 'Products',
      GALLERY: 'Gallery',
      MENULEVELS: 'Menu levels',
      ANIMATIONS: 'Animations',
      LANDING: 'Landing page',
      LAYOUTS: 'Layouts',
      password: 'Password',
      register: 'Register',
      PROFILE: 'Profile',
      rememberMe: 'Remember Me',
      CONTACTS: 'Contacts',
      login: 'Sign in',
      LOGOUT: 'Log out',
      ORDERS: 'Orders',
      ALL_ORDERS: 'All orders',
      PAYMENTS: 'Payments',
      SUPERUSER: 'Super User',
      USERS: 'Users',

      // Define some custom text
      WELCOME: 'Welcome Amelia',
      MESSAGEINFO: 'You have 42 messages and 6 notifications.',
      SEARCH: 'Search...',
      Language: 'Language'

    })
    .translations('ru', {

      // Define all menu elements
      ANALYTICS: 'Аналитика',
      GRAPHS: 'Графики',
      MAILBOX: 'Почта',
      WIDGETS: 'Виджеты',
      METRICS: 'Метрики',
      FORMS: 'Формы',
      APPVIEWS: 'Страницы',
      OTHERPAGES: 'Другие страницы',
      UIELEMENTS: 'UI элементы',
      MISCELLANEOUS: 'Плюшки',
      GRIDOPTIONS: 'Гриды',
      TABLES: 'Таблицы',
      PRODUCTS: 'Продукты',
      GALLERY: 'Галерея',
      MENULEVELS: 'Пункты меню',
      ANIMATIONS: 'Анимация',
      LANDING: 'Посадочная страница',
      LAYOUTS: 'Расположения',
      password: 'Пароль',
      PROFILE: 'Профиль',
      register: 'Зарегистрироваться',
      rememberMe: 'Запомнить меня',
      CONTACTS: 'Контакты',
      login: 'Войти',
      LOGOUT: 'Выйти',
      ORDERS: 'Заказы',
      ALL_ORDERS: 'Все заказы',
      PAYMENTS: 'Оплата',
      SUPERUSER: 'Супер юзер',
      USERS: 'Пользователи',

      // Define some custom text
      WELCOME: 'Добро пожаловать',
      MESSAGEINFO: 'Информация успешно обновлена',
      SEARCH: 'Найти ...',
      Language: 'Язык'
    });

  $translateProvider.preferredLanguage('en');
}


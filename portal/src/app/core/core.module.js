import { coreConfig } from './core.config';
import { configTranslate } from './core.translate.config';
import './services/services.module';

export default angular.module('app.core', [
  /*
   * Angular modules
   */
  'ngAnimate',
  'ngCookies',
  'ngMessages',
  'ngSanitize',
  'LocalStorageModule',
  /*
   * 3rd Party modules
   */
  'ui.bootstrap',             // ui-bootstrap (ex: carousel, pagination, dialog)
  'ui.router',                // uiRouter (states, routes)
  'pascalprecht.translate',   // translate module
  'toastr',                   // notifications
  'ui.grid',
  //'ui.grid.edit',

  /*
   * Our reusable cross app code modules
   */
  'app.core.services'

])
  .config(coreConfig)
  .config(configTranslate);

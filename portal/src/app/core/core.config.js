export function coreConfig (localStorageServiceProvider) {
  'ngInject';

  localStorageServiceProvider
    .setPrefix('meer')
    .setNotify(true, true);
}

function defineBaseUrl() {
  return 'http://localhost:3000'; // port is ok. Just run first node, then angular
}

export const constants = {
  baseURL : defineBaseUrl(),
  states: {
    'root.users': 1,
    'root.user': 1,
    'root.sellers': 2,
    'root.me': 4
  }
};

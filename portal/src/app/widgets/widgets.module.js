import {meerMenu} from './menu.directive';
import {inMinHeight} from './in_min-height.directive';
import {icheck} from './icheck.directive';
import {includeReplace} from './replace.directive';
import {meerCloseNav} from './meer-close-nav.directive';
import * as in_directives from './in_directives.directive';

export default angular.module('app.widgets', ['ui.slimscroll'])
  .directive('inMinHeight', inMinHeight)
  .directive('icheck', icheck)
  .directive('meerMenu', meerMenu)

  // save order in this list (in code it's have the same order)
  .directive('includeReplace', includeReplace)
  .directive('meerCloseNav', meerCloseNav)
  .directive('pageTitle', in_directives.pageTitle)
  .directive('sideNavigation', in_directives.sideNavigation)
  .directive('responsiveVideo', in_directives.responsiveVideo)
  .directive('iboxTools', in_directives.iboxTools)
  .directive('iboxToolsFullScreen', in_directives.iboxToolsFullScreen)
  .directive('minimalizeSidebar', in_directives.minimalizeSidebar)
  .directive('vectorMap', in_directives.vectorMap)
  .directive('sparkline', in_directives.sparkline)
  .directive('ionRangeSlider', in_directives.ionRangeSlider)
  .directive('dropZone', in_directives.dropZone)
  .directive('customValid', in_directives.customValid)
  .directive('fullScroll', in_directives.fullScroll)
  .directive('slimScroll', in_directives.slimScroll)
  .directive('clockPicker', in_directives.clockPicker)
  .directive('fitHeight', in_directives.fitHeight);

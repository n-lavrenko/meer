let body = $('body');

export function pageTitle($rootScope, $timeout) {
  'ngInject';

  return {
    link: function (scope, element) {
      var listener = function (event, toState, toParams, fromState, fromParams) {
        // Default title - load on Dashboard 1
        var title = 'MEER.IO';
        // Create your own title pattern
        if (toState.data && toState.data.pageTitle) {
          title = 'MEER.IO | ' + toState.data.pageTitle;
        }
        $timeout(function () {
          element.text(title);
        });
      };
      $rootScope.$on('$stateChangeStart', listener);
    }
  };
}

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */

export function sideNavigation($timeout) {
  'ngInject';

  return {
    restrict: 'A',
    link: function (scope, element) {
      // Call the metsiMenu plugin and plug it to sidebar navigation
      $timeout(function () {
        //element.metisMenu();

      });

      //Enable initial fixed sidebar
      //var sidebar = element.parent();
      //sidebar.slimScroll({
      //    height: '100%',
      //    railOpacity: 0.9,
      //});
    }
  };
}

/**
 * responsibleVideo - Directive for responsive video
 */
export function responsiveVideo() {
  return {
    restrict: 'A',
    link: function (scope, element) {
      var figure = element;
      var video = element.children();
      video
        .attr('data-aspectRatio', video.height() / video.width())
        .removeAttr('height')
        .removeAttr('width')

      //We can use $watch on $window.innerWidth also.
      $(window).resize(function () {
        var newWidth = figure.width();
        video
          .width(newWidth)
          .height(newWidth * video.attr('data-aspectRatio'));
      }).resize();
    }
  }
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */

export function iboxTools() {
  return {
    restrict: 'A',
    scope: true,
    templateUrl: 'app/views/common/ibox_tools.html',
    controller: iboxToolsController,
    controllerAs: 'vm'
  };
}

//iboxToolsController.$inject = ['$scope', '$element', '$timeout'];
function iboxToolsController($scope, $element, $timeout) {
  'ngInject';

  // Function for collapse ibox
  $scope.showhide = function () {
    var ibox = $element.closest('div.ibox');
    var icon = $element.find('i:first');
    var content = ibox.find('div.ibox-content');
    content.slideToggle(200);
    // Toggle icon from up to down
    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    ibox.toggleClass('').toggleClass('border-bottom');
    $timeout(function () {
      ibox.resize();
      ibox.find('[id^=map-]').resize();
    }, 50);
  };
  // Function for close ibox
  $scope.closebox = function () {
    var ibox = $element.closest('div.ibox');
    ibox.remove();
  }
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */

export function iboxToolsFullScreen() {
  return {
    restrict: 'A',
    scope: true,
    templateUrl: 'app/views/common/ibox_tools_full_screen.html',
    controller: iboxToolsFullScreenController,
    controllerAs: 'vm'
  };
}

//iboxToolsFullScreenController.$inject = ['$scope', '$element', '$timeout'];

function iboxToolsFullScreenController($scope, $element, $timeout) {
  'ngInject';

  // Function for collapse ibox
  $scope.showhide = function () {
    var ibox = $element.closest('div.ibox');
    var icon = $element.find('i:first');
    var content = ibox.find('div.ibox-content');
    content.slideToggle(200);
    // Toggle icon from up to down
    icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    ibox.toggleClass('').toggleClass('border-bottom');
    $timeout(function () {
      ibox.resize();
      ibox.find('[id^=map-]').resize();
    }, 50);
  };
  // Function for close ibox
  $scope.closebox = function () {
    var ibox = $element.closest('div.ibox');
    ibox.remove();
  };
  // Function for full screen
  $scope.fullscreen = function () {
    var ibox = $element.closest('div.ibox');
    var button = $element.find('i.fa-expand');
    $('body').toggleClass('fullscreen-ibox-mode');
    button.toggleClass('fa-expand').toggleClass('fa-compress');
    ibox.toggleClass('fullscreen');
    setTimeout(function () {
      $(window).trigger('resize');
    }, 100);
  }
}

/**
 * minimalizeSidebar - Directive for minimalize sidebar
 */

export function minimalizeSidebar() {
  return {
    restrict: 'E',
    replace: true,
    template: '' +
    '<div class="menu-button" ng-click="vm.toggleMinimalize()" id="close-canvas-menu"></div>',
    controller: minimalizeSidebarController,
    controllerAs: 'vm'
  };
}

function minimalizeSidebarController() {
  /*jshint validthis: true */
  var vm = this;
  vm.toggleMinimalize = toggleMinimalize;

  function toggleMinimalize() {
    body.toggleClass('show-menu');
  }

}

/**
 * vectorMap - Directive for Vector map plugin
 */
export function vectorMap() {
  return {
    restrict: 'A',
    scope: {
      myMapData: '=',
    },
    link: function (scope, element, attrs) {
      element.vectorMap({
        map: 'world_mill_en',
        backgroundColor: "transparent",
        regionStyle: {
          initial: {
            fill: '#e4e4e4',
            "fill-opacity": 0.9,
            stroke: 'none',
            "stroke-width": 0,
            "stroke-opacity": 0
          }
        },
        series: {
          regions: [
            {
              values: scope.myMapData,
              scale: ["#1ab394", "#22d6b1"],
              normalizeFunction: 'polynomial'
            }
          ]
        }
      });
    }
  }
}

/**
 * sparkline - Directive for Sparkline chart
 */
export function sparkline() {
  return {
    restrict: 'A',
    scope: {
      sparkData: '=',
      sparkOptions: '=',
    },
    link: function (scope, element, attrs) {
      scope.$watch(scope.sparkData, function () {
        render();
      });
      scope.$watch(scope.sparkOptions, function () {
        render();
      });
      var render = function () {
        $(element).sparkline(scope.sparkData, scope.sparkOptions);
      };
    }
  }
}

/**
 * ionRangeSlider - Directive for Ion Range Slider
 */
export function ionRangeSlider() {
  return {
    restrict: 'A',
    scope: {
      rangeOptions: '='
    },
    link: function (scope, elem, attrs) {
      elem.ionRangeSlider(scope.rangeOptions);
    }
  }
}

/**
 * dropZone - Directive for Drag and drop zone file upload plugin
 */
export function dropZone() {
  return function (scope, element, attrs) {
    element.dropzone({
      url: "/upload",
      maxFilesize: 100,
      paramName: "uploadfile",
      maxThumbnailFilesize: 5,
      init: function () {
        scope.files.push({file: 'added'});
        this.on('success', function (file, json) {
        });
        this.on('addedfile', function (file) {
          scope.$apply(function () {
            alert(file);
            scope.files.push({file: 'added'});
          });
        });
        this.on('drop', function (file) {
          alert('file');
        });
      }
    });
  }
}


/**
 * customValid - Directive for custom validation example
 */
export function customValid() {
  return {
    require: 'ngModel',
    link: function (scope, ele, attrs, c) {
      scope.$watch(attrs.ngModel, function () {

        // You can call a $http method here
        // Or create custom validation

        var validText = "Inspinia";

        if (scope.extras == validText) {
          c.$setValidity('cvalid', true);
        }
        else {
          c.$setValidity('cvalid', false);
        }

      });
    }
  }
}

/**
 * fullScroll - Directive for slimScroll with 100%
 */

//fullScroll.$inject = ['$timeout'];

export function fullScroll() {

  return {
    restrict: 'A',
    link: function (scope, element) {
      //$timeout(function () {
      //    element.slimscroll({
      //        height: '100%',
      //        railOpacity: 0.9
      //    });
      //
      //});
    }
  };
}

/**
 * slimScroll - Directive for slimScroll with custom height
 */

//slimScroll.$inject = ['$timeout'];

export function slimScroll($timeout) {
'ngInject';

  return {
    restrict: 'A',
    scope: {
      boxHeight: '@'
    },
    link: function (scope, element) {
      $timeout(function () {
        element.slimscroll({
          height: scope.boxHeight,
          railOpacity: 0.9
        });

      });
    }
  };
}

/**
 * clockPicker - Directive for clock picker plugin
 */
export function clockPicker() {
  return {
    restrict: 'A',
    link: function (scope, element) {
      element.clockpicker();
    }
  };
}

/**
 * fitHeight - Directive for set height fit to window height
 */
export function fitHeight() {
  return {
    restrict: 'A',
    link: function (scope, element) {
      element.css("height", $(window).height() + "px");
      element.css("min-height", $(window).height() + "px");
    }
  };
}

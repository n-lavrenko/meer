/**
 * icheck - Directive for custom checkbox icheck
 */

export function icheck($timeout) {
  'ngInject';

  return {
    restrict: 'A',
    require: 'ngModel',
    bindToController: true,
    link: linkFunc
  };

  function linkFunc($scope, element, $attrs, ngModel) {
    return $timeout(function () {
      var value;
      value = $attrs['value'];

      $scope.$watch($attrs['ngModel'], function (newValue) {
        $(element).iCheck('update');
      });

      $scope.$on('$destroy', function () {
        $(element).iCheck('destroy');
      });

      return $(element).iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'

      }).on('ifChanged', function (event) {
        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
          $scope.$apply(function () {
            return ngModel.$setViewValue(event.target.checked);
          });
        }
        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
          return $scope.$apply(function () {
            return ngModel.$setViewValue(value);
          });
        }

        if (typeof $attrs['ngClick'] != 'undefined') {
          $scope.$eval($attrs['ngClick']);
        }

      });
    });
  }

}

export function inMinHeight ($window) {
  'ngInject';

  return {
    link : linkFunc,
    restrict : 'A',
    scope : {}
  };

  function linkFunc(scope, element, attrs) {
    var body = angular.element('body');
    var pageWrapper = angular.element('#page-wrapper');
    var window = angular.element($window);
    var navbar = element;
    var wrapper = angular.element('#wrapper');
    var sidebar = angular.element('.sidebard-panel');

    // Full height
    function fix_height() {
      var heightWithoutNavbar = wrapper.height() - 61;
      sidebar.css("min-height", heightWithoutNavbar + "px");

      var navbarHeigh = navbar.height();
      var wrapperHeigh = pageWrapper.height();

      if (navbarHeigh > wrapperHeigh) {
        pageWrapper.css("min-height", navbarHeigh + "px");
      }

      if (navbarHeigh < wrapperHeigh) {
        pageWrapper.css("min-height", window.height() + "px");
      }

      if (body.hasClass('fixed-nav')) {
        pageWrapper.css("min-height", window.height() - 60 + "px");
      }
    }

    window.bind("load resize scroll", function () {
      if (!body.hasClass('body-small')) {
        fix_height();
      }
    });

    setTimeout(function () {
      fix_height();
    });

    window.bind("load resize", function () {
      if ($(this).width() < 769) {
        body.addClass('body-small')
      }
      else {
        body.removeClass('body-small')
      }
    });
  }

}


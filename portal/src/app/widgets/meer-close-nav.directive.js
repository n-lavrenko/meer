export function meerCloseNav($document) {
  'ngInject';

  let body = $document.find('body');

  return {
    restrict: 'A',
    link: function (scope, el, attrs) {
      let openbtn = $document.find('#close-canvas-menu');

      el.on('click', function (e) {

        var target = angular.element(e.target);

        if (body.hasClass('show-menu') && target !== openbtn) {
          body.removeClass('show-menu');
        }

      });

    }
  };
}

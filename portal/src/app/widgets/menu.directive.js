import {Modernizr} from './multi-level-menu/modernizr-custom';
import {menuLib} from './multi-level-menu/main';

export function meerMenu() {

  return {
    restrict: 'A',
    link: function (scope, el, attrs) {
      let body = angular.element(document).find('body');

      let menuEl = document.getElementById('ml-menu');
      let mlmenu = new MLMenu(menuEl, {
        currentState: scope.$state.current,
        onItemClick: (e, elem) => {
          body.removeClass('show-menu');
        }

      });

    }
  };
}

export function config($httpProvider, $locationProvider, $logProvider, toastrConfig) {
  'ngInject';

  $httpProvider.defaults.useXDomain = true;

  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  // Enable log
  $logProvider.debugEnabled(true);

  $locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    })
    .hashPrefix('!');

  // Set options third-party lib

  angular.extend(toastrConfig, {
    allowHtml : true,
    timeOut: 4000,
    extendedTimeOut: 3000,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    progressBar: true,
    closeButton: true
  });
}

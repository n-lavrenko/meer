/* global malarkey:false, moment:false */

import {config} from './app.config';
import {appRun} from './app.run';
import {constants} from './app.constants';
import './core/core.module';
import './widgets/widgets.module';
import './views/views.module';

angular.module('app', [
    'app.core',
    'app.widgets',
    'app.views'
])

  .config(config)
  .constant(constants)
  .constant('moment', moment)
  .run(appRun);

angular.bootstrap(document, ['app']);

import {configRoutes} from './register.routes';
import {RegisterController} from './register';

export default angular
  .module('app.register', [])
  .config(configRoutes)
  .controller('RegisterController', RegisterController);


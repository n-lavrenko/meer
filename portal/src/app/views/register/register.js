export class RegisterController {
  constructor($auth, $state, toastr, $rootScope, localStorageService) {
    'ngInject';

    this.$state = $state;
    this.loggingIn = false;
    this.credentials = {};
    this.credentials.status_id = '1';
    this.credentials.role_id = '4';
    this.$auth = $auth;
    this.toastr = toastr;
    this.$rootScope = $rootScope;
    this.localStorageService = localStorageService;
  }

  register() {
    this.loggingIn = true;
    this.$auth.signup(this.credentials)
    .then((resRegister) => {
      this.loggingIn = false;
      this.toastr.success('Register completed', 'Success');
      this.$auth.login(this.credentials)
      .then((res) => {
        this.loggingIn = false;
        this.$auth.setToken(res.data.token);

        this.$rootScope.user = res.data.user;
        this.$rootScope.isAdmin = res.data.user.role_id === 1;
        this.$rootScope.userRole = res.data.user.role_id;
        this.$rootScope.isAuthenticated = true;

        this.localStorageService.set('user', res.data.user);
        this.$state.go('root.users');
      })
      .catch((err) => {
        this.loggingIn = false;
        this.toastr.error(err.message, 'Erro');
      });
    })
    .catch((err) => {
      this.loggingIn = false;
      console.log(err.message);
    });
  }

}

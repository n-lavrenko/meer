export function configRoutes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('register', {
      url: '/register',
      templateUrl: 'app/views/register/register.html',
      controller: 'RegisterController',
      controllerAs: 'register'
    });
}

import {configRoutes} from './categories.routes';
import {categoriesSrv} from './categories.service';
import {CategoriesController} from './categories';
import {CategoryController} from './category/category';

export default angular
  .module('app.categories', [])
  .config(configRoutes)
  .service('categoriesSrv', categoriesSrv)
  .controller('CategoriesController', CategoriesController)
  .controller('CategoryController', CategoryController);

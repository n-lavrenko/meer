export class CategoriesController {
  constructor($log, categoriesSrv) {
    'ngInject';

    this.title = 'Categories';
    this.categories = [];
    this.$log = $log;
    this.categoriesSrv = categoriesSrv;
    this.activate();
  }

  activate() {
    this.gridOptions = {
      enableSorting: true,
      enableFiltering: true,
      columnDefs: [
        {field: 'id', displayName: 'Id', width: 70},
        {field: 'name', displayName: 'Name', cellTemplate: '' +
        '<div class="ui-grid-cell-contents">' +
        '<a ui-sref="root.category({id: row.entity.id})">{{row.entity.email }}</a>' +
        '</div>'
        },
        {field: 'user_role.name', displayName: 'Role'},
        {field: 'object_status.name', displayName: 'Status'},
        {field: 'lname', displayName: 'Name', cellTemplate: '' +
        '<div class="ui-grid-cell-contents">' +
        '<a ui-sref="root.category({id: row.entity.id})">{{row.entity.fname + " " + row.entity.lname }}</a>' +
        '</div>'
        },
        {field: 'phone', displayName: 'Phone'},
        {field: 'created_at', displayName: 'Created', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.created_at | date:"dd.MM.yyyy HH:mm:ss" }}</div>'},
        {field: 'updated_at', displayName: 'Updated', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.updated_at | date:"dd.MM.yyyy HH:mm:ss" }}</div>'}
      ]
    };

    this.categoriesSrv.getAll().then((res) => {
      this.categories = res.categories;
      this.categoriesCount = res.count;

      this.gridOptions.data = res.categories;


    }).catch((err) => {
      this.$log.error(err);
    });
  }

}

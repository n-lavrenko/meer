export function configRoutes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('root.categories', {
      url: 'categories',
      templateUrl: 'app/views/categories/categories.html',
      controller: 'CategoriesController as categories'
    })
    .state('root.newCategory', {
      url: 'categories/new',
      templateUrl: 'app/views/categories/category/category.html',
      controller: 'CategoryController as category'
    })
    .state('root.category', {
      url: 'categories/:id',
      templateUrl: 'app/views/categories/category/category.html',
      controller: 'CategoryController as category'
    });

}

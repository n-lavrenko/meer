export class categoriesSrv {
  constructor($http, baseURL, $q, $rootScope) {
    'ngInject';

    this.$http = $http;
    this.$q = $q;
    this.baseURL = baseURL;
    this.$rootScope = $rootScope;
    this.categories = [];
    this.me = undefined;
    this.category = {};
  }

  getAll() {
    var deferred = this.$q.defer();

    if (this.categories.length > 0) {
      deferred.resolve(this.categories);
    }
    else {

      this.$http.get(this.baseURL + '/api/categories')
        .then((res) => {
          this.categories = res.data;
          deferred.resolve(res.data);
        })
        .catch((err) => {
          deferred.reject(err);
        });
    }

    return deferred.promise;
  }

  getCategoryById(id) {
    var deferred = this.$q.defer();

    if (this.category[id]) {
      deferred.resolve(this.category[id]);
    }

    else {
      this.$http.get(this.baseURL + '/api/categories/' + id)
        .then((res) => {
          this.category[id] = res.data;
          deferred.resolve(res.data);
        })
        .catch((err) => {
          deferred.reject(err);
        });
    }

    return deferred.promise;
  }

  updateCategory(category) {
    let req = {
      method: 'PUT',
      url: this.baseURL + '/api/categories/' + category.id,
      data: category
    };

    return this.$http(req);
  }

  deleteCategory(category) {
    return this.$http.delete(this.baseURL + '/api/categories/' + category.id);
  }

}

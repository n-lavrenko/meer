export class LoginController {
  constructor($auth, $rootScope, $state, $log, localStorageService,  toastr) {
    'ngInject';

    this.credentials = {};
    this.loggingIn = false;
    this.$auth = $auth;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$log = $log;
    this.localStorageService = localStorageService;
    this.toastr = toastr;
  }

  login() {
    this.loggingIn = true;
    this.$auth.login(this.credentials)
      .then((res) => {
        this.loggingIn = false;
        this.$auth.setToken(res.data.token);

        this.$rootScope.user = res.data.user;
        this.$rootScope.isAdmin = res.data.user.role_id === 1;
        this.$rootScope.userRole = res.data.user.role_id;
        this.$rootScope.isAuthenticated = true;

        this.localStorageService.set('user', res.data.user);
        this.toastr.success('Logged in as ' + res.data.user.email, 'Success');


        this.$state.go('root.users');
      })
      .catch((err) => {
        this.toastr.error(err.data.message, 'Error');
        this.loggingIn = false;
        this.credentials = {};
        this.$log.error(err);
      });
  }

  authenticate(provider) {
    this.$auth.authenticate(provider);
  }

}

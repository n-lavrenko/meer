export function configRoutes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: 'app/views/login/login.html',
      controller: 'LoginController',
      controllerAs: 'login'
    });
}

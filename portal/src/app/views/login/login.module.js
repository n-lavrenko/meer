import {configRoutes} from './login.routes';
import {LoginController} from './login';

export default angular.module('app.login', [])
  .config(configRoutes)
  .controller('LoginController', LoginController);

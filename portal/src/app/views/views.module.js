import {routerConfig} from './views.routes';
import {MainController} from './main';
import './login/login.module';
import './register/register.module';
import './users/users.module';
import './categories/categories.module';

export default angular.module('app.views', [
  'app.login',
  'app.register',
  'app.users',
  'app.categories'
])
  .config(routerConfig)
  .controller('MainController', MainController);

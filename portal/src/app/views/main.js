export class MainController {
  constructor($rootScope, $translate, $auth, $state, localStorageService) {
    'ngInject';

    this.currentLang = 'en';
    this.$rootScope = $rootScope;
    this.$translate = $translate;
    this.$auth = $auth;
    this.$state = $state;
    this.localStorageService = localStorageService;
    this.activate();
  }

  activate() {
    this.$rootScope.isWaiting = false;
    this.$rootScope.isGrey = false;
  }

  setLanguage(langCode) {
    this.currentLang = langCode;
    this.$translate.use(langCode);
  }

  logout() {
    this.$auth.logout();
    this.localStorageService.clearAll();
    angular.element(document).find('body').removeClass('show-menu');
    this.$rootScope.isAuthenticated = false;
    this.$rootScope.user = null;
    this.$rootScope.userRole = null;
    this.$rootScope.isAdmin = false;

    this.$state.go('login');
  }
}

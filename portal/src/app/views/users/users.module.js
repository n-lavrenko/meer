import {configRoutes} from './users.routes';
import {userSrv} from './users.service';
import {UsersController} from './users';
import {UserController} from './user/user';

export default angular
  .module('app.users', [])
  .config(configRoutes)
  .service('userSrv', userSrv)
  .controller('UsersController', UsersController)
  .controller('UserController', UserController);

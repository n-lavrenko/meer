export class userSrv {
  constructor($http, baseURL, $q, $rootScope) {
    'ngInject';

    this.$http = $http;
    this.$q = $q;
    this.baseURL = baseURL;
    this.$rootScope = $rootScope;
    this.users = [];
    this.me = undefined;
    this.user = {};
  }

  getAll() {
    var deferred = this.$q.defer();

    if (this.users.length > 0) {
      deferred.resolve(this.users);
    }
    else {

      this.$http.get(this.baseURL + '/api/users')
        .then((res) => {
          this.users = res.data;
          deferred.resolve(res.data);
        })
        .catch((err) => {
          deferred.reject(err);
        });
    }

    return deferred.promise;
  }

  getUserById(id) {
    var deferred = this.$q.defer();

    if (this.user[id]) {
      deferred.resolve(this.user[id]);
    }

    else {
      this.$http.get(this.baseURL + '/api/users/' + id)
        .then((res) => {
          this.user[id] = res.data;
          deferred.resolve(res.data);
        })
        .catch((err) => {
          deferred.reject(err);
        });
    }

    return deferred.promise;
  }

  getMe() {
    var deferred = this.$q.defer();

    if (this.me) {
      this.$rootScope.user = this.me;
      deferred.resolve(this.me);
    }

    else {
      this.$http.get(this.baseURL + '/api/me')
        .then((res) => {
          this.me = res.data;
          this.$rootScope.user = res.data;
          deferred.resolve(res.data);
        })
        .catch((err) => {
          deferred.reject(err);
        });
    }

    return deferred.promise;
  }

  updateUser(user) {
    let req = {
      method: 'PUT',
      url: this.baseURL + '/api/users/' + user.id,
      data: user
    };

    return this.$http(req);
  }

  deleteUser(user) {
    return this.$http.delete(this.baseURL + '/api/users/' + user.id);
  }

}

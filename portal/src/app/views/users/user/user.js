export class UserController {
  constructor(userSrv, $log, $rootScope, toastr, $auth) {
    'ngInject';
    this.title = 'User';
    this.userSrv = userSrv;
    this.$auth = $auth;
    this.toastr = toastr;
    this.$rootScope = $rootScope;
    this.$log = $log;
    this.user = {};
    this.isMe = this.$rootScope.$state.current.name === 'root.me';
    this.isNew = this.$rootScope.$state.current.name === 'root.newUser';
    this.activate();
  }

  activate() {
    if (this.isMe) {
      this.userSrv.getMe().then((res) => {
        this.user = res;
      }).catch((err) => {
        this.$log.error(err.message || err);
      });
    }
    else if (!this.isNew) {
      this.userSrv.getUserById(this.$rootScope.$stateParams.id).then((res) => {
        this.user = res;
      }).catch((err) => {
        this.$log.error(err.message || err);
        this.$rootScope.$state.go(this.$rootScope.$state.fromState.name);
      });
    }

  }

  updateUser() {
    this.userSrv.updateUser(this.user).then((res) => {
      this.toastr.success('Updated', 'Success');
      if (this.isMe) {
        this.$rootScope.user = this.user;
      }

      }).catch((err) => {
      this.$log.error(err);
      this.toastr.error(err.message, 'Error');
    });
  }

  createUser() {
    this.$auth.signup(this.user)
      .then((user) => {
        this.toastr.success('User has been created', 'Success');
        this.$rootScope.$state.go('root.users');
      }).catch((err) => {
        this.$log.error(err.message || err);
        this.toastr.error(err.message || err, 'Error');
    });
  }

  deleteUser() {
    this.userSrv.deleteUser(this.user).then((res) => {
      this.toastr.success('Deleted', 'Success');
      this.$rootScope.$state.go('root.users');
    }).catch((err) => {
      this.$log.error(err.message || err);
      this.toastr.error(err.message || err, 'Error');
    });
  }

}

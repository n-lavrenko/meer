export function configRoutes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('root.users', {
      url: 'users',
      templateUrl: 'app/views/users/users.html',
      controller: 'UsersController as users'
    })
    .state('root.newUser', {
      url: 'users/new',
      templateUrl: 'app/views/users/user/user.html',
      controller: 'UserController as user'
    })
    .state('root.user', {
      url: 'users/:id',
      templateUrl: 'app/views/users/user/user.html',
      controller: 'UserController as user'
    })
    .state('root.me', {
      url: 'me',
      templateUrl: 'app/views/users/user/user.html',
      controller: 'UserController as user'
    });

}

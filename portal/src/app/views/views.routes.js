export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider
    .state('root', {
      url: '/',
      abstract: true,
      templateUrl: 'app/views/layout/layout.html',
      controller: 'MainController',
      controllerAs: 'main'
    });

  $urlRouterProvider.otherwise('/login');

}

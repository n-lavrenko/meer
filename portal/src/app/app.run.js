export function appRun($rootScope, $state, $injector, $stateParams) {
  'ngInject';

  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  $rootScope.isAuthenticated = $injector.get('$auth').isAuthenticated();

  if ($rootScope.isAuthenticated) {
    let localStorageService = $injector.get('localStorageService');

    $rootScope.user = localStorageService.get('user');
    $rootScope.isAdmin = $rootScope.user.role_id === 1;
    $rootScope.userRole = $rootScope.user.role_id;
  }


  let states = $injector.get('states'); // from app.constants.js

  $rootScope.$on('$stateChangeStart', function stateChangeStart(event, toState) {

    if (!($rootScope.isAuthenticated) && (toState.name !== 'login' && toState.name !== 'register')) {
      event.preventDefault();
      $state.go('login');
    }

    else if ($rootScope.isAuthenticated && (toState.name === 'login' || toState.name === 'register')) {
      event.preventDefault();
      $state.go('root.me');
    }

    else if (states[toState.name] < $rootScope.userRole) {
      event.preventDefault(); // Prevent migration to default state
      $state.go('root.me');
    }

  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    $rootScope.$state.fromState = fromState;
    $rootScope.$state.fromParams = fromParams;
    $rootScope.$state.toState = toState;
    $rootScope.$state.toParams = toParams;
  });

  // Check for state change errors.
  $rootScope.$on('$stateChangeError', function stateChangeError(event, toState, toParams, fromState, fromParams, error) {

    //$injector.get('MessageService')
    //  .error('Error loading the page');

    //$state.get('error').error = {
    //  event: event,
    //  toState: toState,
    //  toParams: toParams,
    //  fromState: fromState,
    //  fromParams: fromParams,
    //  error: error
    //};

    //return $state.go('error');
  });
}

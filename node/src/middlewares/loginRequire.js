'use strict';

const config = require('../config');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const userSrv = require('../services/userSrv');

let previousTokensList = {};

module.exports = (req, res, next) => {

  /*
   |--------------------------------------------------------------------------
   | Login Required Middleware
   |--------------------------------------------------------------------------
   */

  let token = req.header('Access-Token');
  let currentMoment = +moment().format('x');

  if (!token) {
    return res.status(401).json({message: 'Please make sure your request has an Authorization header'});
  }

  let parsed = null;
  try {
    parsed = jwt.verify(token, config.secret);
  }
  catch (err) {
    return res.status(401).send({message: err.message});
  }

  if (parsed.exp <= currentMoment) {
    return res.status(401).send({message: 'Token has expired'});
  }

  if (typeof previousTokensList[parsed.uid] === 'undefined') {
    previousTokensList[parsed.uid] = {};
    previousTokensList[parsed.uid].tokens = [];
  }
  else if (previousTokensList[parsed.uid].tokens.indexOf(token) !== -1 &&
    currentMoment - previousTokensList[parsed.uid].lastDate > 30000) {
    return res.status(401).send({message: 'Token has been used. Please, relogin.'});
  }

  req.uid = parsed.uid;

  let newToken = userSrv.createJWT(parsed.uid);

  res.setHeader('Access-Control-Expose-Headers', 'Access-Token, Token-Type');
  res.setHeader('Token-Type', 'Bearer');
  res.setHeader('Cache-Control', 'max-age=0, private, must-revalidate');
  res.setHeader('Access-Token', newToken);

  if (previousTokensList[parsed.uid].tokens.indexOf(token) === -1) {
    previousTokensList[parsed.uid].tokens.push(token); // used Token / blacklist tokens
  }
  previousTokensList[parsed.uid].lastDate = currentMoment;

  next();

};
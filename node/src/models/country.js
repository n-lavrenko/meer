/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('country', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name_text_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'text',
        key: 'id'
      }
    },
    code: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'country',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false,
    classMethods: {associate: function(models) {
        this.belongsTo(models.Text, {foreignKey: 'name_text_id'});
      }
    }
  });
};

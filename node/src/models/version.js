/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('version', {
    entity: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    client_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'provider',
        key: 'id'
      }
    },
    version: {
      type: DataTypes.STRING,
      allowNull: false
    },
    options: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'version',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false
  });
};

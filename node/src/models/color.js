/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('color', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name_text_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description_text_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'object_status',
        key: 'id'
      }
    }
  }, {
    tableName: 'color',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false
  });
};

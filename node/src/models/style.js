/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('style', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'style',
    freezeTableName: true,
    underscored: true
  });
};

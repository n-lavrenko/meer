/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_permission', {
    user_role_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user_role',
        key: 'id'
      }
    },
    permitted_action_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user_action',
        key: 'id'
      }
    }
  }, {
    tableName: 'user_permission',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false
  });
};

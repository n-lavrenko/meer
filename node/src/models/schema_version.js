/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('schema_version', {
    version_rank: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    installed_rank: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    version: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    script: {
      type: DataTypes.STRING,
      allowNull: false
    },
    checksum: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    installed_by: {
      type: DataTypes.STRING,
      allowNull: false
    },
    execution_time: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    success: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'schema_version',
    freezeTableName: true,
    underscored: true
  });
};

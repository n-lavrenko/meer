/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('currency', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    code: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name_text_id: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'currency',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false
  });
};

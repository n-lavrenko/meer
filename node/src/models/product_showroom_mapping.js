/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product_showroom_mapping', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    look_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'showroom',
        key: 'id'
      }
    },
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'product',
        key: 'id'
      }
    },
    x: {
      type: 'REAL',
      allowNull: false
    },
    y: {
      type: 'REAL',
      allowNull: false
    },
    width: {
      type: 'REAL',
      allowNull: false
    },
    z_index: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'product_showroom_mapping',
    freezeTableName: true,
    underscored: true
  });
};

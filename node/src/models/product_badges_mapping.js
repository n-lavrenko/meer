/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product_badges_mapping', {
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'product',
        key: 'id'
      }
    },
    badge_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'badge',
        key: 'id'
      }
    }
  }, {
    tableName: 'product_badges_mapping',
    freezeTableName: true,
    underscored: true
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('showroom_like', {
    showroom_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'showroom',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    }
  }, {
    tableName: 'showroom_like',
    freezeTableName: true,
    underscored: true
  });
};

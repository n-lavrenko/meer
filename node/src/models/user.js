/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    provider_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'provider',
        key: 'id'
      }
    },
    seller_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'seller',
        key: 'id'
      }
    },
    role_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user_role',
        key: 'id'
      }
    },
    status_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'object_status',
        key: 'id'
      }
    },
    fname: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'First Name'
    },
    lname: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'Last Name'
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    country_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'country',
        key: 'id'
      }
    },
    city_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'city',
        key: 'id'
      }
    },
    region_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'region',
        key: 'id'
      }
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    updated_by: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    underscored: true,
    tableName: 'user',
    freezeTableName: true,
    classMethods: {
      associate: function(models) {
        this.belongsTo(models.Role, {foreignKey: 'role_id'}); // todo: as not working
        this.belongsTo(models.Status, {foreignKey: 'status_id'}); // todo: as not working
        this.belongsTo(models.Country, {foreignKey: 'country_id'}); // todo: as not working
        this.belongsTo(models.City, {foreignKey: 'city_id'}); // todo: as not working
        this.belongsTo(models.Region, {foreignKey: 'region_id'}); // todo: as not working
      }
    }
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('showroom_badges_mapping', {
    look_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'showroom',
        key: 'id'
      }
    },
    badge_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'badge',
        key: 'id'
      }
    }
  }, {
    tableName: 'showroom_badges_mapping',
    freezeTableName: true,
    underscored: true
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('style_product_mapping', {
    style_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'style',
        key: 'id'
      }
    },
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'product',
        key: 'id'
      }
    }
  }, {
    tableName: 'style_product_mapping',
    freezeTableName: true,
    underscored: true
  });
};

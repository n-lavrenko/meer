/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('text', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    text_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      unique: false
    },
    language_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'language',
        key: 'id'
      }
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    updated_by: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'text',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false
  });
};

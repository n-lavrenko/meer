/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('black_list_token', {
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
      primaryKey: true
    }
  }, {
    underscored: true,
    tableName: 'black_list_token',
    freezeTableName: true
  });
};

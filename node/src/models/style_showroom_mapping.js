/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('style_showroom_mapping', {
    style_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'style',
        key: 'id'
      }
    },
    showroom_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'showroom',
        key: 'id'
      }
    }
  }, {
    tableName: 'style_showroom_mapping',
    freezeTableName: true,
    underscored: true
  });
};

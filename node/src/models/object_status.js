/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('object_status', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    tableName: 'object_status',
    freezeTableName: true,
    underscored: true,
    updatedAt: false,
    createdAt: false
  });
};

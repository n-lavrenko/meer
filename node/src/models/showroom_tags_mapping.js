/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('showroom_tags_mapping', {
    showroom_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'showroom',
        key: 'id'
      }
    },
    tag_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tags',
        key: 'id'
      }
    }
  }, {
    tableName: 'showroom_tags_mapping',
    freezeTableName: true,
    underscored: true
  });
};

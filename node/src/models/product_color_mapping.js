/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product_color_mapping', {
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'product',
        key: 'id'
      }
    },
    color_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'color',
        key: 'id'
      }
    }
  }, {
    tableName: 'product_color_mapping',
    freezeTableName: true,
    underscored: true
  });
};

'use strict';

const sequelize = require('../config/sequelize');
const models = {};

// user
models.User = sequelize.import('./user');
models.Role = sequelize.import('./user_role');
models.UserAction = sequelize.import('./user_action');
models.UserPermission = sequelize.import('./user_permission');
models.Provider = sequelize.import('./provider');
models.Seller = sequelize.import('./seller');
models.Client = sequelize.import('./client_target');
models.Store = sequelize.import('./store');

// global
models.Status = sequelize.import('./object_status');
models.Language = sequelize.import('./language');
models.Currency = sequelize.import('./currency');
models.Country = sequelize.import('./country');
models.City = sequelize.import('./city');
models.Region = sequelize.import('./region');
models.Category = sequelize.import('./category');
models.BlackListToken = sequelize.import('./black_list_token');
models.SchemaVersion = sequelize.import('./schema_version');
models.Version = sequelize.import('./version');
models.Text = sequelize.import('./text');

// product
models.SubCategory = sequelize.import('./sub_category');
models.Badge = sequelize.import('./badge');
models.ProductGroup = sequelize.import('./product_group');
models.ProductImages = sequelize.import('./product_images');
models.ProductPrice = sequelize.import('./product_price');
models.Product = sequelize.import('./product');
models.Brand = sequelize.import('./brand');
models.Collection = sequelize.import('./collection');
models.Color = sequelize.import('./color');
models.Material = sequelize.import('./material');
models.Style = sequelize.import('./style');
models.Tags = sequelize.import('./tags');

// showroom
models.Showroom = sequelize.import('./showroom');
models.ShowroomLike = sequelize.import('./showroom_like');

// mapping
models.ProductBadgesMapping = sequelize.import('./product_badges_mapping');
models.ProductColorMapping = sequelize.import('./product_color_mapping');
models.ProductShowroomMapping = sequelize.import('./product_showroom_mapping');
models.ShowroomBadgesMapping = sequelize.import('./showroom_badges_mapping');
models.ShowroomTagsMapping = sequelize.import('./showroom_tags_mapping');
models.StyleProductMapping = sequelize.import('./style_product_mapping');
models.StyleShowroomMapping = sequelize.import('./style_showroom_mapping');


models.Country.associate(models);
models.User.associate(models);

//sequelize.sync({force: true}).then(() => {console.log('db was droped and created');});
//sequelize.sync().then(() => {console.log('db was droped and created');});

module.exports = models;

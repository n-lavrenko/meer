'use strict';

const userSrv = require('../services/userSrv');

module.exports = (req, res, next) => {


  if (typeof req.body.email === 'undefined' || typeof req.body.password === 'undefined' ) {
    let message = { message: 'Authentication failed. Fill email & password'};
    res.status(400).json(message);
    return false;
  }

  userSrv.login(req.body, res, next);

};
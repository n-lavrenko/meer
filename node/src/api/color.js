'use strict';
exports.post = function(req, res) {
  const ColorSrv = new require('../services/colorSrv')(req, res);

  ColorSrv.createOne(req, function(err, user) {
    if (err) {
      if (err instanceof AuthError) {
        return next(new Error(403, err.message));
      } else {
        return next(err);
      }
    }

    req.session.user = user._id;
    res.send({});

  });
};

exports.get = function(req, res) {
  const ColorSrv = require('../services/colorSrv');
  ColorSrv.findAll(req, res);
};
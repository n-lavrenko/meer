'use strict';

const userSrv = require('../services/userSrv');

module.exports = (req, res, next) => {

  if (typeof req.body.email === 'undefined' || typeof req.body.password === 'undefined' ) {
    res.json({ success: false, message: 'Authentication failed. Fill email & password' });
    next();
  }

  userSrv.createUser(req.body, res, next);

};
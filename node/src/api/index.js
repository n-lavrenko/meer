'use strict';
//let expressJwt = require('express-jwt');

let config = require('../config');
let loginRequire = require('../middlewares/loginRequire');


module.exports = (app) => {

  app.post('/auth/login', require('./login'));
  app.post('/auth/signup', require('./register'));


  // protected
  app.use('/api/*', loginRequire);

  app.get('/api/me', require('./user').getMe);
  app.get('/api/users/:id', require('./user').getUserById);
  app.put('/api/users/:id', require('./user').updateUserById);
  app.delete('/api/users/:id', require('./user').deleteUserById);

  app.get('/api/users', require('./user').getAll);

  app.get('/api/user_role', require('./user_role').getAll);
  app.get('/api/status', require('./status').getAll);

};
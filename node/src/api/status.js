'use strict';

const statusSrv = require('../services/statusSrv');

module.exports.getAll = (req, res, next) => {

  statusSrv.getAll(req.body, res, next);

};
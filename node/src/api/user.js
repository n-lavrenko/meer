'use strict';

const userSrv = require('../services/userSrv');

module.exports.getAll = (req, res, next) => {
  userSrv.getAll(req, res, next);
};

module.exports.getMe = (req, res, next) => {
  userSrv.getMe(req, res, next);
};

module.exports.getUserById = (req, res, next) => {
  userSrv.getUserById(req, res, next);
};

module.exports.updateUserById = (req, res, next) => {
  userSrv.updateUserById(req, res, next);
};

module.exports.deleteUserById = (req, res, next) => {
  userSrv.deleteUserById(req, res, next);
};
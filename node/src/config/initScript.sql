INSERT INTO "user_role"("id", "name", "weight") VALUES('1', 'super', '100') RETURNING "id", "name", "weight";
INSERT INTO "user_role"("id", "name", "weight") VALUES('2', 'provider', '50') RETURNING "id", "name", "weight";
INSERT INTO "user_role"("id", "name", "weight") VALUES('3', 'seller', '40') RETURNING "id", "name", "weight";
INSERT INTO "user_role"("id", "name", "weight") VALUES('4', 'client', '30') RETURNING "id", "name", "weight";
INSERT INTO "user_role"("id", "name", "weight") VALUES('5', 'anonim', '10') RETURNING "id", "name", "weight";


INSERT INTO "object_status"("id", "name", "description") VALUES('1', 'active', 'active status') RETURNING "id", "name", "description";
INSERT INTO "object_status"("id", "name", "description") VALUES('2', 'inactive', 'inactive status') RETURNING "id", "name", "description";
INSERT INTO "object_status"("id", "name", "description") VALUES('3', 'deleted', 'deleted status') RETURNING "id", "name", "description";


INSERT INTO "user_action"("id", "name", "description") VALUES('1', 'structure-edit', 'editable of app structure: color, roles, statuses') RETURNING "id", "name", "description";
INSERT INTO "user_action"("id", "name", "description") VALUES('2', 'create', 'create action') RETURNING "id", "name", "description";
INSERT INTO "user_action"("id", "name", "description") VALUES('3', 'delete', 'delete action') RETURNING "id", "name", "description";
INSERT INTO "user_action"("id", "name", "description") VALUES('4', 'edit', 'edit action') RETURNING "id", "name", "description";
INSERT INTO "user_action"("id", "name", "description") VALUES('5', 'read', 'read action') RETURNING "id", "name", "description";


INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('1', '1') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('1', '2') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('1', '3') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('1', '5') RETURNING "user_role_id", "permitted_action_id";

INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('2', '2') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('2', '3') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('2', '4') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('2', '5') RETURNING "user_role_id", "permitted_action_id";

INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('3', '4') RETURNING "user_role_id", "permitted_action_id";
INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('3', '5') RETURNING "user_role_id", "permitted_action_id";

INSERT INTO "user_permission"("user_role_id", "permitted_action_id") VALUES('4', '5') RETURNING "user_role_id", "permitted_action_id";


INSERT INTO "language"("id", "name", "code") VALUES('1', 'English', 'en_EN') RETURNING "id", "name", "code";
INSERT INTO "language"("id", "name", "code") VALUES('2', 'Russian', 'ru_RU') RETURNING "id", "name", "code";


INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('1', '1', '1', 'Nikita', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('2', '2', '1', 'Lavrenko', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('3', '3', '1', 'Dollar US', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('4', '4', '1', 'Russion RUB', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('5', '5', '1', 'Belarus RUB', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('6', '6', '1', 'Belarus', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('7', '7', '1', 'Russia', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('8', '8', '1', 'Poland', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('9', '9', '1', 'red', '1', '1');
INSERT INTO "text"("id", "text_id", "language_id", "text", "created_by", "updated_by") VALUES('10', '10', '1', 'its just red color', '1', '1');


INSERT INTO "currency"("id", "code", "name_text_id") VALUES('1', '$', '3') RETURNING "id", "code", "name_text_id";
INSERT INTO "currency"("id", "code", "name_text_id") VALUES('2', 'RUR', '4') RETURNING "id", "code", "name_text_id";
INSERT INTO "currency"("id", "code", "name_text_id") VALUES('3', 'BYR', '5') RETURNING "id", "code", "name_text_id";


INSERT INTO "country"("id", "name_text_id", "code") VALUES('1', '6', 'BLR') RETURNING "id", "name_text_id", "code";
INSERT INTO "country"("id", "name_text_id", "code") VALUES('2', '7', 'RUS') RETURNING "id", "name_text_id", "code";
INSERT INTO "country"("id", "name_text_id", "code") VALUES('3', '8', 'RUS') RETURNING "id", "name_text_id", "code";

INSERT INTO "user" ("id","email","password","role_id","status_id","updated_at","created_at") VALUES (DEFAULT,'lavrenonik@gmail.com','7fd2e3e78198fb2faa52246525abe97a35afc00c92128afa5d82175c995bb4cf','1','1','2016-03-30 21:11:44.283 +00:00','2016-03-30 21:11:44.283 +00:00') RETURNING *;


INSERT INTO "color"("id", "name_text_id", "description_text_id", "created_by", "updated_by", "status_id") VALUES('1', '9', '10', '1', '1', '1');
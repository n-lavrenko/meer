'use strict';

const models = require('../models');
const config = require('../config');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const moment = require('moment');

class userSrv {

  createJWT(userId) {
    let payload = {
      uid: userId,
      now: +moment().format('x'),
      exp: +moment().add(3, 'days').format('x')
    };
    return jwt.sign(payload, config.secret);
  }

  encryptPassword(password) {
    return crypto.createHmac('sha256', config.secret)
      .update(password)
      .digest('hex');
  }

  checkPassword(password, hashedPassword) {
    return this.encryptPassword(password) === hashedPassword;
  }

  newToken(email) {
    return jwt.sign(email, config.secret);
  }

  createUser(form, res) {
    //const hashedPassword = this.encryptPassword(form.password);

    form.password = this.encryptPassword(form.password);
    models.User.create(form)
      .then((result) => {

        delete form.password;
        delete form.repassword;
        res.status(200).json(form);

      })
      .catch((err) => {
        console.log(err);
        res.status(500).json(err);
      });
  }

  login(form, res, next) {

    models.User.findOne({
        where: {email: form.email},
        attributes: ['id', 'email', ['password', 'hashedpassword'], 'role_id']
      })
      .then((result) => {
        if (!result) {
          let message = {message: 'Wrong user or password'};
          res.status(401).send(message);
          return false;
        }
        else {

          let user = result.dataValues;

          if (this.checkPassword(form.password, user.hashedpassword)) {

            delete user.hashedpassword;

            let token = this.createJWT(user.id);

            res.setHeader('Token-Type', 'Bearer');
            res.setHeader('Cache-Control', 'max-age=0, private, must-revalidate');
            res.setHeader('Access-Token', token);

            res.status(200).json({
              user: user,
              token: token
            });

          }
          else {
            res.status(401).json({message: 'Wrong user or password'});
          }
        }

      })

      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });

  }

  getAll(req, res, next) {

    models.User.findAndCountAll({
        attributes: {exclude: ['password']},
        include: [models.Status, models.Role, models.Country, models.City, models.Region],
        order: 'id ASC'
      })
      .then((result) => {

        if (result.count > 0) {
          res.json({
            count: result.count,
            users:result.rows
          });
        }
        else {
          res.status(404).json({error: 'Nothing found'});
        }
      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });
  }

  getUserById(req, res, next) {
    models.User.findOne({
        where: {id: req.params.id},
        attributes: {exclude: ['password']},
        include: [models.Status, models.Role, models.Country, models.City, models.Region]
      })
      .then((result) => {
        if (!result) {
          res.status(404).json({message: 'Nothing found'});
        }

        res.json(result.dataValues);

      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });
  }

  getMe(req, res, next) {
    models.User.findOne({
        where: {id: req.uid},
        attributes: {exclude: ['password']},
        include: [models.Status, models.Role, models.Country, models.City, models.Region]
      })
      .then((result) => {
        if (!result) {
          res.status(404).json({message: 'Nothing found'});
        }

        res.json(result.dataValues);

      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });
  }

  updateUserById(req, res, next) {
    models.User.update(
      req.body, {
        where: {id: req.params.id}
      })
      .then((result) => {
        if (!result) {
          res.status(404).json({message: 'Nothing found'});
        }

        res.sendStatus(200);

      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });
  }

  deleteUserById(req, res, next) {
    models.User.destroy({
        where: {id: req.params.id}
      })
      .then((result) => {
        if (!result) {
          res.status(404).json({message: 'Nothing found'});
        }

        res.sendStatus(200);

      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });
  }

}

module.exports = new userSrv;
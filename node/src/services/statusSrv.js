'use strict';

const sequelize = require('../config/sequelize');
const Status = sequelize.import('../models/object_status');

class statusSrv {

  getAll(req, res, next) {
    Status.findAndCountAll({
        attributes: ['id', 'name', 'description']
      })
      .then((result) => {
        if (!result) {
          res.status(404).json({message: 'Nothing found'});
        }

        if (result.count > 0) {
          res.json({
            count: result.count,
            statuses: result.rows
          });
        }
        else {
          res.status(404).json({error: 'Nothing found'});
        }

      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });
  }

}

module.exports = new statusSrv;
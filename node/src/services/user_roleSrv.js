'use strict';

const sequelize = require('../config/sequelize');
const User_role = sequelize.import('../models/user_role');

class user_roleSrv {

  getAll(req, res, next) {
    User_role.findAndCountAll({
        attributes: ['id', 'name', 'weight']
      })
      .then((result) => {
        if (!result) {
          res.status(404).json({message: 'Nothing found'});
        }

        if (result.count > 0) {
          res.json({
            count: result.count,
            roles: result.rows
          });
        }
        else {
          res.status(404).json({error: 'Nothing found'});
        }

      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(500);
      });

  }

}

module.exports = new user_roleSrv;
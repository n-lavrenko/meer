export function dropable() {

  return {
    restrict: 'A',
    link: (scope, el, attrs) => {

      el = el[0];
      el.draggable = true;

      el.addEventListener(
        'dragstart', (e) => {
          e.dataTransfer.setData('imgPrev', e.target.currentSrc);
          e.dataTransfer.setData('imgSrc', attrs.txtSrc);
          e.dataTransfer.setData('imgWidth', attrs.imgTxtWidth);
          e.dataTransfer.setData('imgHeight', attrs.imgTxtHeight);

          e.dataTransfer.effectAllowed = 'move';
        }
      );
    }
  };
}

//import {THREE} from '../../three.js/build/three-es6';
import {THREE} from './webgl/build/three_old';

//import '../../three.js/build/OrbitControls';
import './webgl/js/controls/OrbitControls';

//import '../../three.js/build/DDSLoader';
import './webgl/js/loaders/DDSLoader';

//import '../../three.js/build/MTLLoader';
import './webgl/js/loaders/MTLLoader';

//import '../../three.js/build/OBJLoader';
import './webgl/js/loaders/OBJLoader';
import './webgl/js/loaders/OBJMTLLoader';

//import '../../three.js/build/Detector';
import './webgl/js/Detector';

export function webview($, _, showroomService, $timeout) {
  'ngInject';

  return {
    restrict: 'EA',
    templateUrl: 'app/widgets/webview.html',

    link: (scope, el, attrs) => {

      let baseURL = 'app/widgets/webgl/_Showroom/';

      scope.isShowRoomLoading = false;
      scope.showroomSizes = showroomService.getSizes();
      scope.showroomStyles = showroomService.getStyles();
      scope.showroomOptions = showroomService.getOptions();

      scope.showrooms = showroomService.getAllShowrooms();
      scope.selectedShowroom = scope.showrooms[0];

      scope.selShowroomSize = scope.showroomSizes[0];
      scope.selShowroomStyle = scope.showroomStyles[0];
      scope.selShowroomOption = scope.showroomOptions[0];

      scope.shPath = scope.selectedShowroom.url;

      scope.mainpath = baseURL + scope.shPath;

      let camera,
        scene,
        renderer,
        canvas,

        // camera
        VIEW_ANGLE = 45,

        NEAR = 1,
        FAR = 50000,

        mouse = new THREE.Vector2(), INTERSECTED,
        //raycaster,
        pickingGeometry,
        pickingTexture,
        pickingScene,
        cameraControls,
        texture,
        sidesTypes = ['top', 'front', 'left', 'right', 'back', 'bottom'];


      let defaultValue = 1000;

      scope.sides = {
        top: {
          TxPath: baseURL + 'TEXTURES/top/textured/001.jpg',
          ObjTx: scope.mainpath + '/texture/_TOP.obj',
          TxWidth: defaultValue,
          TxHeight: defaultValue
        },
        front: {
          TxPath: baseURL + 'TEXTURES/front/textured/white.jpg',
          ObjTx: scope.mainpath + '/texture/_FRONT.obj',
          TxWidth: defaultValue,
          TxHeight: defaultValue
        },
        left: {
          TxPath: baseURL + 'TEXTURES/left/textured/white.jpg',
          ObjTx: scope.mainpath + '/texture/_LEFT.obj',
          TxWidth: defaultValue,
          TxHeight: defaultValue
        },
        right: {
          TxPath: baseURL + 'TEXTURES/right/textured/white.jpg',
          ObjTx: scope.mainpath + '/texture/_RIGHT.obj',
          TxWidth: defaultValue,
          TxHeight: defaultValue
        },
        back: {
          TxPath: baseURL + 'TEXTURES/back/textured/white.jpg',
          ObjTx: scope.mainpath + '/texture/_BACK.obj',
          TxWidth: defaultValue,
          TxHeight: defaultValue
        },
        bottom: {
          TxPath: baseURL + 'TEXTURES/bottom/textured/001.jpg',
          ObjTx: scope.mainpath + '/texture/_BOTTOM.obj',
          TxWidth: 1600,
          TxHeight: 1600
        }
      };

      scope.selectShowroom = (showroom) => {
        scope.selectedShowroom = showroom;
        scope.shPath = showroom.url;
        scope.mainpath = baseURL + scope.shPath;
        sidesTypes.map(side => {
          scope.sides[side].ObjTx = scope.mainpath + '/texture/_' + _.upperCase(side) + '.obj';
        });
      };

        // global loading manager
      let manager = new THREE.LoadingManager();

      function initTextureModel(side) {
        let texture;
        let loaderTxt = new THREE.ImageLoader(manager);
        let loaderObj = new THREE.OBJLoader(manager);

        texture = new THREE.Texture();

        loaderTxt.load(scope.sides[side].TxPath, function (image) {

          texture.image = image;
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping;

          texture.repeat.x = defaultValue / scope.sides[side].TxWidth;
          texture.repeat.y = defaultValue / scope.sides[side].TxHeight;

          texture.needsUpdate = true;

        });

        loaderObj.load(scope.sides[side].ObjTx, function (object) {

          if (side === 'bottom') {
            let materialBottom = new THREE.MeshPhongMaterial({
              map: texture,
              specular: 0xa0a0a0,
              shininess: 15
            });

            object.castShadow = false;
            object.receiveShadow = true;

            object.traverse(function (child) {
              if (child instanceof THREE.Mesh) {
                child.name = side;
                child.material = materialBottom;
              }
            });
          }

          else {
            object.traverse(function (child) {
              if (child instanceof THREE.Mesh) {
                child.name = side;
                child.material.map = texture;
              }
            });
          }

          object.name = side + '_textureModel';
          scene.add(object);

        }, onProgress, onError);

      }

      function initDragAndDrop(side) {
        let dragArea = $('#holder-' + side);

        dragArea
          .on('dragover', (e) => {
            e.preventDefault();
            e.stopPropagation();
            $(this).addClass('dragging');
          })
          .on('dragenter', (e) => {
            e.preventDefault();
            e.stopPropagation();
          })
          .on('dragleave', (e) => {
            e.preventDefault();
            e.stopPropagation();
            $(this).removeClass('dragging');
          })
          .on('drop', (e) => {
            e.preventDefault();
            e.stopPropagation();

            let imgSrc;
            let dropedPreviewSrc, dropedImageSrc, dropedTxtWidth, dropedTxtHeight;

            function readFromOS() {

              let file = e.originalEvent.dataTransfer.files[0];

              if (typeof window.FileReader === 'undefined') {
                console.error('Filereader not supported');
                return;
              }
              let reader = new FileReader();

              reader.onload = (event) => {
                initNewTexture(event.target.result);
              };
              reader.readAsDataURL(file);
            }

            function initNewTexture(previewUrl, imgUrl, imgWidth, imgHeight) {
              dragArea.css('background-image', 'url(' + previewUrl + ')');

              let image = document.createElement('img');
              image.src = imgUrl;
              let texture = new THREE.Texture(image);

              texture.needsUpdate = true;
              scene.getObjectByName(side).material.map = texture;

              scene.getObjectByName(side).material.map.wrapS = scene.getObjectByName(side).material.map.wrapT = THREE.RepeatWrapping;
              scene.getObjectByName(side).material.map.repeat.x = 1000 / imgWidth;
              scene.getObjectByName(side).material.map.repeat.y = 1000 / imgHeight;
            }


            if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length > 0) {
              readFromOS();
            }

            else {
              dropedPreviewSrc = e.originalEvent.dataTransfer.getData('imgPrev');
              dropedImageSrc = e.originalEvent.dataTransfer.getData('imgSrc');
              if (dropedImageSrc.includes ('wall&deco')) {
                if (scope.mainpath.includes ('1S')) {
                  dropedTxtHeight = 2410;
                  dropedTxtWidth = (e.originalEvent.dataTransfer.getData('imgWidth') /
                                    e.originalEvent.dataTransfer.getData('imgHeight')) * 2410;}
                else {
                    if (scope.mainpath.includes ('2M')) {
                      dropedTxtHeight = 2710;
                      dropedTxtWidth = (e.originalEvent.dataTransfer.getData('imgWidth') /
                        e.originalEvent.dataTransfer.getData('imgHeight')) * 2710;
                    }
                    else {

                      if (scope.mainpath.includes ('3L')) {
                        dropedTxtHeight = 2710;
                        dropedTxtWidth = (e.originalEvent.dataTransfer.getData('imgWidth') /
                          e.originalEvent.dataTransfer.getData('imgHeight')) * 2710;
                      }
                    }
                }

              }
              else {
                dropedTxtWidth = e.originalEvent.dataTransfer.getData('imgWidth');
                dropedTxtHeight = e.originalEvent.dataTransfer.getData('imgHeight');
              }

              initNewTexture(dropedPreviewSrc, dropedImageSrc, dropedTxtWidth, dropedTxtHeight);
            }

          });

      }


      function initSides() {
        sidesTypes.map(side => {
          initTextureModel(side);
          initDragAndDrop(side);
        });
      }


      function init() {
        let WIDTH = $(window).width() - 600,
          HEIGHT = $(window).height(),
          ASPECT = WIDTH / HEIGHT;

        // renderer
        renderer = new THREE.WebGLRenderer({antialias: true});

        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(WIDTH, HEIGHT);
        renderer.setClearColor(0xffffff);

        $(renderer.domElement).css('left', 'calc(50% - ' + WIDTH / 2 + 'px)');

        // scene
        scene = new THREE.Scene();

        pickingGeometry = new THREE.Geometry();

        pickingScene = new THREE.Scene();
        pickingTexture = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight);
        pickingTexture.minFilter = THREE.LinearFilter;
        pickingTexture.generateMipmaps = false;

        // camera
        camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
        camera.position.set(0, 8500, 0);
        //camera.position.set(-4000, 2000, 0);

        cameraControls = new THREE.OrbitControls(camera, renderer.domElement);
        cameraControls.target.set(0, 0, 0);
        cameraControls.maxDistance = 50000;
        cameraControls.minDistance = -100000;
        cameraControls.update();

        $(el).find('#canvas-container')[0].appendChild(renderer.domElement);

        let light = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.9);

        scene.add(light);

        let spotLight = new THREE.SpotLight(0xffffff, 1, 7000, Math.PI / 2, 8, 1);
        spotLight.position.set(0, 1500, -5000);
        spotLight.target.position.set(0, 1500, 0);
        spotLight.scale.x = 5;

        scene.add(spotLight);

      }

      function onProgress(xhr) {
        if (xhr.lengthComputable) {
          let percentComplete = xhr.loaded / xhr.total * 100;
          console.log( Math.round(percentComplete) + '% downloaded' );
        }
      }

      function onError(xhr) {
        console.log(xhr);
      }

      function fillScene() {
        // Grid

        // FURNITURE ALL___________________________________________________

        let OBJMTLLoader = new THREE.OBJMTLLoader();
        OBJMTLLoader.load(
          scope.mainpath + '/furniture/_ALL.obj',
          scope.mainpath + '/furniture/_ALL.mtl',
          function (object2) {

            object2.name = 'object2';
            scene.add(object2);

            initSides();

            initAllShadow();

          }, onProgress, onError);

        // ALL SHADOWS texture

        function initAllShadow() {
          let textureAllSh = new THREE.Texture();

          let loaderShAll = new THREE.ImageLoader(manager);
          loaderShAll.load(scope.mainpath + '/shadow/_SHADOW_CompleteMap.jpg', function (image) {

            textureAllSh.image = image;
            textureAllSh.needsUpdate = true;

          });

          let materialAll = new THREE.MeshBasicMaterial({map: textureAllSh});
          materialAll.transparent = true;
          materialAll.blending = THREE.MultiplyBlending;

          // All SHADOWS model

          let loaderAllShadow = new THREE.OBJMTLLoader(manager);
          loaderAllShadow.load(
            scope.mainpath + '/shadow/_SHADOW.obj',
            scope.mainpath + '/shadow/_SHADOW.mtl',
            function (object) {

              object.traverse(function (child) {
                if (child instanceof THREE.Mesh) {
                  child.material = materialAll;
                }

              });

              object.receiveShadow = true;

              object.name = 'shadowObject';
              scene.add(object);

              $timeout(() => {
                scope.isShowRoomLoading = false;
              }, 500);


            }, onProgress, onError);
        }

      }

      function render() {
        renderer.render(scene, camera);
      }

      function update() {
        requestAnimationFrame(update);

        // ROTATO______&_______MOVATO_____________________________

        //var timer = Date.now() * 0.00005;
        //scene.rotation.y = ( Math.PI / 2 ) - timer * 0.1;
        //scene.position.z = 0 - timer * 0.00001;

        cameraControls.update();
        render();
      }

      function canvasInit() {
        if (typeof renderer === 'undefined') {
          init();
        }
        else {
          sidesTypes.map(side => {
            var selectedObject = scene.getObjectByName(side + '_textureModel');
            scene.remove(selectedObject);
          });
          var shadowObject = scene.getObjectByName('shadowObject');
          var object2 = scene.getObjectByName('object2');

          scene.remove(shadowObject);
          scene.remove(object2);
        }
        fillScene();
        update();
      }

      scope.initWebGl = () => {
        scope.isShowRoomLoading = true;
        $timeout(() => {
          scope.isSelectOpen = false;
        }, 700);
        scope.loadedShowroomUrl = angular.copy(scope.selectedShowroom).url;
        canvasInit();
      };

    }
  };
}

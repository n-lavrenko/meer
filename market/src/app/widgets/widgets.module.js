import {includeReplace} from './replace.directive';
import {splitter} from './splitter.directive';
import {dropable} from './dropable.directive';
import {webview} from './webview.directive';

export default angular.module('meer.widgets', [])
  .directive('includeReplace', includeReplace)
  .directive('dropable', dropable)
  .directive('splitter', splitter)
  .directive('webview', webview);

export function splitter($, $interval) {
  'ngInject';

  return {
    restrict: 'A',
    link: function () {

      var $webViewContainer = $('#webview-container');

      let contentWidth;

      let headerLogo = $('#logo-container'), headerLogoWidth,
        footer = $('#footer');

      function initMainBlocksWidths() {
        let interval = $interval(() => {
          let contentContainer = $('#main-container');
          let width = contentContainer.outerWidth();
          if (width > 10) {
            contentContainer.width(width - 50);
            contentWidth = width;
            $interval.cancel(interval);
          }
        }, 200);

        let interval2 = $interval(() => {
          headerLogo = $('#logo-container');
          footer = $('#footer');
          let width = headerLogo.width();
          if (width > 10) {
            headerLogo.width(width - 100);
            footer.width(width - 100);
            headerLogoWidth = width;
            $interval.cancel(interval2);
          }
        }, 200);
      }

      initMainBlocksWidths();

      $webViewContainer.resizable({
        handles: 'n, e, s, w',
        maxWidth: $(window).outerWidth() - 450,
        minWidth: 100,
        resize: function(event, ui) {
          var splitWidth = ui.size.width;
          var $content = $('#main-container');

          $(this).width(splitWidth);

          headerLogo = $('#logo-container');
          footer = $('#footer');

          headerLogo.width(headerLogoWidth - splitWidth);
          footer.width(headerLogoWidth - splitWidth);
          $content.width(contentWidth - splitWidth + 50);

        }
      });

      /*$(window).off('resize.split');
      $(window).on('resize.split', () => {
        let contentContainer = $('#main-container');
        contentWidth = $(this).width() - 200 - $webViewContainer.width();
        $webViewContainer.resizable.maxWidth = $(window).width() - 500;
      });*/

    }
  };
}

export function appConfig ($httpProvider, $locationProvider, $logProvider, toastrConfig, $provide) {
  'ngInject';

  Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item);
  };

  $provide.decorator('$state', function($delegate, $rootScope) {
    'ngInject';

    $rootScope.$on('$stateChangeStart', function(event, state, params) {
      $delegate.next = state;
      $delegate.toParams = params;
    });

    return $delegate;
  });

  $httpProvider.defaults.useXDomain = true;
  $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
  // $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
  // $httpProvider.defaults.headers.common['Access-Control-Request-Headers'] = 'accept, accept-encoding, accept-language, access-control-request-headers, access-control-request-method, connection, dnt, host, origin, referer, user-agent';
  // $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';




  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  // Enable log
  $logProvider.debugEnabled(true);

  $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    })
    .hashPrefix('!');

  // Set options third-party lib

  angular.extend(toastrConfig, {
    allowHtml : true,
    timeOut: 4000,
    extendedTimeOut: 3000,
    positionClass: 'toast-top-right',
    preventDuplicates: false,
    progressBar: true,
    closeButton: true
  });

}

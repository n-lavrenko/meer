export function appRun($rootScope, $state, $stateParams, authService) {
  'ngInject';

  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  function redirectToDefaultState(event){
    event.preventDefault();
  }

  $rootScope.$on('$stateChangeStart', function (event, toState) {
    let data = toState.data;
    let token = authService.getToken();

    if(data && data.protected) {
      if (token === undefined) {
        redirectToDefaultState(event);
      }
    }

    if (token && toState.name === 'public.auth.login') {
      event.preventDefault();
      $state.go('protected.market.category');
    }
  });

  // Check for state change errors.
  /*$rootScope.$on('$stateChangeError', function stateChangeError(event, toState, toParams, fromState, fromParams, error) {
    event.preventDefault();
    // todo: handle this case
  });*/
}

import {authRoutes} from './auth.routes';
import {LoginController} from './login/login.controller';

export default angular.module('meer.views.auth', [])
  .config(authRoutes)
  .controller('LoginController', LoginController);

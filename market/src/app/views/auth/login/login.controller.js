export class LoginController {
  constructor(authService, toastr, $state) {
    'ngInject';

    this.authService = authService;
    this.$state = $state;
    this.toastr = toastr;
  }

  login() {
    this.authService.simpleAuth(this.username, this.password)
      .then(res => {
        this.toastr.success(res);
        this.$state.go('protected.market.category');
      })
      .catch(err => {
        this.toastr.error(err);
      });
  }

}

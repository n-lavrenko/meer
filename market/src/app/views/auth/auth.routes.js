export function authRoutes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('public.auth', {
      abstract: true,
      views: {
        'layout@' : {
          templateUrl: 'app/views/auth/auth.layout.html'
        }
      }
    })
    .state('public.auth.login', {
      url: '/login',
      views: {
        'content': {
          templateUrl: 'app/views/auth/login/login.html',
          controller: 'LoginController as vm'
        }
      }
    });
}

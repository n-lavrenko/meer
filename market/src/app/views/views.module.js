import {routerConfig} from './views.routes';
import './market/market.module';
import './auth/auth.module';

export default angular.module('meer.views', [
  'meer.views.market',
  'meer.views.auth'
])
  .config(routerConfig);

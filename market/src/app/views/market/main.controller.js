export class MainController {
  constructor(authService, $state) {
    'ngInject';

    this.authService = authService;
    this.$state = $state;
  }

  logout() {
    this.authService.logout();
    this.$state.go('public.auth.login');
  }

}

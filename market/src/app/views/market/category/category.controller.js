export class CategoryController {
  constructor(productService, authService, $, _, $state) {
    'ngInject';

    this.productService = productService;
    this.authService = authService;
    this.$state = $state;

    this.$ = $;
    this._ = _;
    this.activate();
  }

  activate() {
    this.productService.getBrands()
      .then(res => {
        this.brands = res;
        this.selectCollection(this.brands[0].id, this.brands[0].collections[0]);
      });
  }

  selectCollection(brand, collection) {
    this.selectedCollection = collection;
    this.productService.getCollection(brand, collection)
      .then(res => {
        res.map(product => {

          let urlArr = this._.split(product.src, '/');
          urlArr.insert(-1, 'Prev');

          product.txtSrc = product.src;
          product.src = urlArr.join('/');

          product.article = this._.split(urlArr[urlArr.length - 1], '.')[0];

        });
        this.products = res;
      });
  }

}

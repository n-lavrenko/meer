export function marketRoutes($stateProvider) {
  'ngInject';

  $stateProvider
    .state('protected.market', {
      abstract: true,
      views: {
        'layout@' : {
          templateUrl: 'app/views/market/market-layout.html'
        },
        'header@protected.market' : {
          templateUrl: 'app/views/market/header.html'
        },
        'footer@protected.market' : {
          templateUrl: 'app/views/market/footer.html'
        }
      }
    })
    .state('protected.market.category', {
      url: '/category',
      views: {
        'content': {
          templateUrl: 'app/views/market/category/category.html',
          controller: 'CategoryController as vm'
        }
      },
      data: {
        stateName: 'Catalog'
      }
    });
}

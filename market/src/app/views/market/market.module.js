import {marketRoutes} from './market.routes';
//import {HomeController} from './home/home.controller';
import {CategoryController} from './category/category.controller';
import {MainController} from './main.controller';

//import './template/js/owl.carousel.min.js';
//import './template/js/bootstrap.min.js';
//import './template/js/chosen.jquery.min.js';
//import './template/js/Modernizr.js';
import './template/js/jquery-ui.min.js';
//import './template/js/lightbox.min.js';
//import './template/js/masonry.pkgd.min.js'; // todo: fix all commented files: errors adm loading
//import './template/js/imagesloaded.pkgd.min.js';
//import './template/js/isotope.pkgd.min.js';
//import './template/js/jquery.mCustomScrollbar.concat.min.js';
//import './template/js/jquery.parallax-1.1.3.js';
//import './template/js/jquery.magnific-popup.min.js';
//import './template/js/masonry.js';
//import './template/js/functions.js';


export default angular.module('meer.views.market', [])
  .config(marketRoutes)
  .controller('MainController', MainController)
  .controller('CategoryController', CategoryController);
  //.controller('HomeController', HomeController)

export function routerConfig($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider
    .state('protected', {
      abstract: true,
      data: {
        protected: true
      }
    })
    .state('public', {
      abstract: true,
      data: {
        protected: false
      }
    });

  $urlRouterProvider.otherwise('/login');

}

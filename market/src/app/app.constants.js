export const constants = {
  baseUrl : 'http://localhost:3000',
  authEndPoints: {
    token: 'http://api.meer.io/auth/token',
    logout: 'http://api.meer.io/auth/logout'
  },
  brandsAccess: {
    'Seabrook': ['seabrook', 'york', 'kahrs'],
    'New-Walls': ['wallanddeco', 'kahrs'],
    'Edelis': ['seabrook', 'york', 'kahrs'],
    'Imperiya-Pola': ['seabrook', 'wallanddeco', 'york']
  }
};

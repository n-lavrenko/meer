import { coreConfig } from './core.config';
import './services/services.module';

export default angular.module('meer.core', [
  /*
   * Angular modules
   */
  'ngAnimate',
  'ngStorage',
  /*
   * 3rd Party modules
   */
  'ui.router',          // uiRouter (states, routes)
  'toastr',             // notifications

  /*
   * Our reusable cross app code modules
   */

  'meer.core.services'

])
  .config(coreConfig);

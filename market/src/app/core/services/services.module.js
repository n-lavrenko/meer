import {servicesConfig} from './services.config';
import './auth/auth.module';
import './user-data-storage/user-data-storage.module';
import './product/product.module';
import './showroom/showroom.module';

export default angular.module('meer.core.services', [
  'meer.core.services.auth',
  'meer.core.services.user-data-storage',
  'meer.core.services.product',
  'meer.core.services.showroom'
])
  .config(servicesConfig);

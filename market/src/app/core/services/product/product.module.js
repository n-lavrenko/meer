import {ProductRepository} from './product.repository';
import {ProductService} from './product.service';

export default angular.module('meer.core.services.product', [])
  .service('productRepository', ProductRepository)
  .service('productService', ProductService);

export class ProductService {

  constructor(productRepository, authService, brandsAccess, _) {
    'ngInject';

    this.productRepository = productRepository;
    this.authService = authService;
    this.brandsAccess = brandsAccess;
    this._ = _;
  }

  getBrands() {
    return this.productRepository.getBrands()
      .then(res => {
        let username = this.authService.getUsername();
        let access = this.brandsAccess[username];
        if (access) {
          res.data = this._.filter(res.data, brand => {
            return access.indexOf(brand.id) !== -1;
          });
        }
        return res.data;
      });
  }

  getCollection(brand, collectionName) {
    return this.productRepository.getCollection(brand, collectionName)
      .then(res => res.data);
  }


}


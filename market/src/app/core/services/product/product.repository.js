export class ProductRepository {
  constructor($q, $http) {
    'ngInject';

    this.$q = $q;
    this.$http = $http;
  }

  getBrands() {
    return this.$http.get('../assets/JSONs/brands.json')
      .then(res => res);
  }

  getCollection(brand, collectionName) {
    return this.$http.get(`../assets/JSONs/${brand}/${collectionName}.json`)
      .then(res => res);
  }

}

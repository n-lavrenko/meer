import {UserDataStorage} from './user-data-storage.service';

export default angular.module('meer.core.services.user-data-storage', [])
  .service('userDataStorage', UserDataStorage);

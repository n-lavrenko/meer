class UserModel {
  constructor(token, userName, firstName, lastName) {
    this.token = token;
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}

export class UserDataStorage {

  constructor($localStorage) {
    'ngInject';

    this.localStorage = $localStorage;

    this.storage = this.localStorage;

    this.storage.userData || (this.storage.userData = new UserModel());
  }

  deleteUser() {
    let userName = this.getUserName();
    delete this.storage.userData;
    this.storage.userData = new UserModel();
    this.setUserName(userName);
  }

  getToken() {
    return this.storage.userData.token;
  }

  setToken(token) {
    this.storage.userData.token = token;
  }

  getUserName() {
    return this.storage.userData.userName;
  }

  setUserName(userName) {
    this.storage.userData.userName = userName;
  }

}

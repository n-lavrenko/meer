import {myHttpInterceptor} from './http.interceptors';

export function servicesConfig($httpProvider) {
  'ngInject';

  // http handler
  $httpProvider.interceptors.push(myHttpInterceptor);

}

export class AuthService {

  constructor(authRepository, userDataStorage) {
    'ngInject';

    this.authRepository = authRepository;
    this.userDataStorage = userDataStorage;
  }

  simpleAuth(userName, password) {
    return this.authRepository.token(userName, password)
      .then(res => {
        this.setToken(res);
        this.setUserName(userName);
        return 'You have successfully logged in';
      });
  }

  getToken() {
    return this.userDataStorage.getToken();
  }

  setToken(token) {
    this.userDataStorage.setToken(token);
  }

  setUserName(username) {
    this.userDataStorage.setUserName(username);
  }

  getUsername() {
    return this.userDataStorage.getUserName();
  }

  logout() {
    this.userDataStorage.deleteUser();
    return this.authRepository.logout().finally(() => {

    });
  }

}


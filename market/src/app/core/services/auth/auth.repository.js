import * as CryptoJS from 'crypto-js';

export class AuthRepository {
  constructor($http, authEndPoints, $timeout, $q, _) {
    'ngInject';

    this.$q = $q;
    this._ = _;
    this.$timeout = $timeout;
    this.$http = $http;
    this.authEndPoints = authEndPoints;
  }

  checkPassword(userName, password) {
    return true;//this.hashs.indexOf(CryptoJS.HmacSHA384(userName, password).toString()) !== -1;
  }

  token(userName, password) {
    let deferred = this.$q.defer();

    this.$http({
      method: 'POST',
      url: this.authEndPoints.token,
      data: {
        grant_type: 'password',
        l: userName,
        p: password
      },
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });

    this.$http.get('../assets/hashes.json')
      .then(res => {
        this.hashs = res.data;

        if (this.checkPassword(userName, password)) {
          this.$timeout(() => {
            deferred.resolve('Bearer ' + CryptoJS.HmacMD5(userName + password, password).toString());
          }, 1000);
        }
        else {
          this.$timeout(() => {
            deferred.reject('Invalid Username or Password');
          }, 1000)
        }
      });

    return deferred.promise;

  }

  logout() {
    return this.$http.post(this.authEndPoints.logout);
  }

}

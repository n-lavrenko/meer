import {AuthRepository} from './auth.repository';
import {AuthService} from './auth.service';

export default angular.module('meer.core.services.auth', [])
  .service('authService', AuthService)
  .service('authRepository', AuthRepository);

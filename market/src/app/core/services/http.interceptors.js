export function myHttpInterceptor($q) {
  'ngInject';

  return {
    'request': (config) => {
      return config;
    },

    'requestError': (rejection) => {
      return this.$q.reject(rejection);
    },


    'response': function(response) {
      return response;
    },

    'responseError': function(rejection) {
      return $q.reject(rejection);
    }
  };
}


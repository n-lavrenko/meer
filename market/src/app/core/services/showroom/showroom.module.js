import {ShowroomRepository} from './showroom.repository';
import {ShowroomService} from './showroom.service';

export default angular.module('meer.core.services.showroom', [])
  .service('showroomRepository', ShowroomRepository)
  .service('showroomService', ShowroomService);

export class ShowroomRepository {
  constructor($q) {
    'ngInject';

    this.$q = $q;
    this.showrooms = [];

    this.sizes = [
      {
        id: '1S',
        name: 'S'
      }, {
        id: '2M',
        name: 'M'
      }, {
        id: '3L',
        name: 'L'
      }
    ];

    this.styles = [
      {
        id: '1Min',
        name: 'Minimalism'
      },
      {
        id: '2MCL',
        name: 'Modern Classic'
      },
      {
        id: '3Scn',
        name: 'Scandinavian'
      },
      {
        id: '4Loft',
        name: 'Loft'
      },
      {
        id: '5Cas',
        name: 'Casual'
      },
      {
        id: '6ECL',
        name: 'Eclectic'
      }
    ];

    this.options = [
      {
        id: '_1',
        name: 1
      },
      {
        id: '_2',
        name: 2
      },
      {
        id: '_3',
        name: 3
      }
    ];
  }

  getSizes() {
    return this.sizes;
  }

  getStyles() {
    return this.styles;
  }

  getOptions() {
    return this.options;
  }

  getAllShowrooms() {
    this.generateShoowrooms();
    return this.showrooms;
  }

  generateShoowrooms() {
    this.getSizes().map(size => {
      this.getStyles().map(style => {
        this.getOptions().map(option => {
          if (style.id === '6ECL' && size.name === 'L') return;
          this.showrooms.push(new Showroom(size, style, option));
        });
      });
    });
  }
}

class Showroom {
  constructor(size, style, option) {
    this.size = size;
    this.style = style;
    this.option = option;
    this.url = `${style.id}/${size.id}/${option.id}`;
  }
}

export class ShowroomService {

  constructor(showroomRepository) {
    'ngInject';

    this.showroomRepository = showroomRepository;
  }

  getSizes() {
    return this.showroomRepository.getSizes();
  }

  getStyles() {
    return this.showroomRepository.getStyles();
  }

  getOptions() {
    return this.showroomRepository.getOptions();
  }

  getAllShowrooms() {
    return this.showroomRepository.getAllShowrooms();
  }


}


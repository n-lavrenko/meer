'use strict';
import {appConfig} from './app.config';
import {appRun} from './app.run';
import {constants} from './app.constants';

import './core/core.module';
import './widgets/widgets.module';
import './views/views.module';

angular.module('meer', [
    'meer.core',
    'meer.widgets',
    'meer.views'
])
  .config(appConfig)
  .constant(constants)
  .constant('_', window._)
  .constant('$', window.jQuery)
  .run(appRun);

angular.bootstrap(document, ['meer']);
